export interface IAddress {
  id: number;
  description: string;
  floor: string;
  street: string;
  locality: string;
  postal_code: string;
  region: string;
  country: string;
  geom: string;
}
